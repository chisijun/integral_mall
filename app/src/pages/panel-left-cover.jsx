import React from "react";
import nv_fy from "../img/nv-fy.jpg";
import seckill_1 from "../img/seckill_1.jpg";
import {
  List,
  BlockTitle,
  ListItem,
  Page,
  Navbar,
  AccordionContent,
  Block
} from "framework7-react";

export default () => (
  <Page>
    <Navbar title="分类浏览" />
    <BlockTitle>全部分类</BlockTitle>
  <List accordionList inset>
    <ListItem accordionItem title="为您推荐">
      <AccordionContent>
        <List>
          <ListItem title="连衣裙">
          <img
            slot="media"
            alt=""
            src={seckill_1}
            width="120px"
          />
          </ListItem>
          <ListItem title="T恤">
          <img
            slot="media"
            alt=""
            src={seckill_1}
            width="120px"
          />
          </ListItem>
          <ListItem title="衬衫">
          <img
            slot="media"
            alt=""
            src={seckill_1}
            width="120px"
          />
          </ListItem>
          <ListItem title="雪纺衫">
          <img
            slot="media"
            alt=""
            src={seckill_1}
            width="120px"
          />
          </ListItem>
          <ListItem title="裤子">
          <img
            slot="media"
            alt=""
            src={seckill_1}
            width="120px"
          />
          </ListItem>
          <ListItem title="牛仔裤">
          <img
            slot="media"
            alt=""
            src={seckill_1}
            width="120px"
          />
          </ListItem>
        </List>
      </AccordionContent>
    </ListItem>
    <ListItem accordionItem title="国际大牌">
      <AccordionContent>
        <List>
          <ListItem title="Item 1"></ListItem>
          <ListItem title="Item 2"></ListItem>
          <ListItem title="Item 3"></ListItem>
          <ListItem title="Item 4"></ListItem>
        </List>
      </AccordionContent>
    </ListItem>
    <ListItem accordionItem title="明誉教育">
      <AccordionContent>
        <List>
          <ListItem title="Item 1"></ListItem>
          <ListItem title="Item 2"></ListItem>
          <ListItem title="Item 3"></ListItem>
          <ListItem title="Item 4"></ListItem>
        </List>
      </AccordionContent>
    </ListItem>
    <ListItem accordionItem title="图书">
      <AccordionContent>
        <List>
          <ListItem title="Item 1"></ListItem>
          <ListItem title="Item 2"></ListItem>
          <ListItem title="Item 3"></ListItem>
          <ListItem title="Item 4"></ListItem>
        </List>
      </AccordionContent>
    </ListItem>
    <ListItem accordionItem title="女装">
      <AccordionContent>
        <List>
          <ListItem title="Item 1"></ListItem>
          <ListItem title="Item 2"></ListItem>
          <ListItem title="Item 3"></ListItem>
          <ListItem title="Item 4"></ListItem>
        </List>
      </AccordionContent>
    </ListItem>
    <ListItem accordionItem title="女鞋">
      <AccordionContent>
        <List>
          <ListItem title="Item 1"></ListItem>
          <ListItem title="Item 2"></ListItem>
          <ListItem title="Item 3"></ListItem>
          <ListItem title="Item 4"></ListItem>
        </List>
      </AccordionContent>
    </ListItem>
    <ListItem accordionItem title="男装">
      <AccordionContent>
        <List>
          <ListItem title="Item 1"></ListItem>
          <ListItem title="Item 2"></ListItem>
          <ListItem title="Item 3"></ListItem>
          <ListItem title="Item 4"></ListItem>
        </List>
      </AccordionContent>
    </ListItem>
    <ListItem accordionItem title="男鞋">
      <AccordionContent>
        <List>
          <ListItem title="Item 1"></ListItem>
          <ListItem title="Item 2"></ListItem>
          <ListItem title="Item 3"></ListItem>
          <ListItem title="Item 4"></ListItem>
        </List>
      </AccordionContent>
    </ListItem>
    <ListItem accordionItem title="内衣">
      <AccordionContent>
        <List>
          <ListItem title="Item 1"></ListItem>
          <ListItem title="Item 2"></ListItem>
          <ListItem title="Item 3"></ListItem>
          <ListItem title="Item 4"></ListItem>
        </List>
      </AccordionContent>
    </ListItem>
    <ListItem accordionItem title="母婴">
      <AccordionContent>
        <List>
          <ListItem title="Item 1"></ListItem>
          <ListItem title="Item 2"></ListItem>
          <ListItem title="Item 3"></ListItem>
          <ListItem title="Item 4"></ListItem>
        </List>
      </AccordionContent>
    </ListItem>
    <ListItem accordionItem title="手机">
      <AccordionContent>
        <List>
          <ListItem title="Item 1"></ListItem>
          <ListItem title="Item 2"></ListItem>
          <ListItem title="Item 3"></ListItem>
          <ListItem title="Item 4"></ListItem>
        </List>
      </AccordionContent>
    </ListItem>
    <ListItem accordionItem title="数码">
      <AccordionContent>
        <List>
          <ListItem title="Item 1"></ListItem>
          <ListItem title="Item 2"></ListItem>
          <ListItem title="Item 3"></ListItem>
          <ListItem title="Item 4"></ListItem>
        </List>
      </AccordionContent>
    </ListItem>
    <ListItem accordionItem title="家电">
      <AccordionContent>
        <List>
          <ListItem title="Item 1"></ListItem>
          <ListItem title="Item 2"></ListItem>
          <ListItem title="Item 3"></ListItem>
          <ListItem title="Item 4"></ListItem>
        </List>
      </AccordionContent>
    </ListItem>
    <ListItem accordionItem title="美妆">
      <AccordionContent>
        <List>
          <ListItem title="Item 1"></ListItem>
          <ListItem title="Item 2"></ListItem>
          <ListItem title="Item 3"></ListItem>
          <ListItem title="Item 4"></ListItem>
        </List>
      </AccordionContent>
    </ListItem>
    <ListItem accordionItem title="箱包">
      <AccordionContent>
        <List>
          <ListItem title="Item 1"></ListItem>
          <ListItem title="Item 2"></ListItem>
          <ListItem title="Item 3"></ListItem>
          <ListItem title="Item 4"></ListItem>
        </List>
      </AccordionContent>
    </ListItem>
    <ListItem accordionItem title="运动">
      <AccordionContent>
        <List>
          <ListItem title="Item 1"></ListItem>
          <ListItem title="Item 2"></ListItem>
          <ListItem title="Item 3"></ListItem>
          <ListItem title="Item 4"></ListItem>
        </List>
      </AccordionContent>
    </ListItem>
    <ListItem accordionItem title="户外">
      <AccordionContent>
        <List>
          <ListItem title="Item 1"></ListItem>
          <ListItem title="Item 2"></ListItem>
          <ListItem title="Item 3"></ListItem>
          <ListItem title="Item 4"></ListItem>
        </List>
      </AccordionContent>
    </ListItem>
    <ListItem accordionItem title="家装">
      <AccordionContent>
        <List>
          <ListItem title="Item 1"></ListItem>
          <ListItem title="Item 2"></ListItem>
          <ListItem title="Item 3"></ListItem>
          <ListItem title="Item 4"></ListItem>
        </List>
      </AccordionContent>
    </ListItem>
    <ListItem accordionItem title="家纺">
      <AccordionContent>
        <List>
          <ListItem title="Item 1"></ListItem>
          <ListItem title="Item 2"></ListItem>
          <ListItem title="Item 3"></ListItem>
          <ListItem title="Item 4"></ListItem>
        </List>
      </AccordionContent>
    </ListItem>
    <ListItem accordionItem title="居家百货">
      <AccordionContent>
        <List>
          <ListItem title="Item 1"></ListItem>
          <ListItem title="Item 2"></ListItem>
          <ListItem title="Item 3"></ListItem>
          <ListItem title="Item 4"></ListItem>
        </List>
      </AccordionContent>
    </ListItem>
    <ListItem accordionItem title="鲜花宠物">
      <AccordionContent>
        <List>
          <ListItem title="Item 1"></ListItem>
          <ListItem title="Item 2"></ListItem>
          <ListItem title="Item 3"></ListItem>
          <ListItem title="Item 4"></ListItem>
        </List>
      </AccordionContent>
    </ListItem>
    <ListItem accordionItem title="配饰">
      <AccordionContent>
        <List>
          <ListItem title="Item 1"></ListItem>
          <ListItem title="Item 2"></ListItem>
          <ListItem title="Item 3"></ListItem>
          <ListItem title="Item 4"></ListItem>
        </List>
      </AccordionContent>
    </ListItem>
    <ListItem accordionItem title="食品">
      <AccordionContent>
        <List>
          <ListItem title="Item 1"></ListItem>
          <ListItem title="Item 2"></ListItem>
          <ListItem title="Item 3"></ListItem>
          <ListItem title="Item 4"></ListItem>
        </List>
      </AccordionContent>
    </ListItem>
    <ListItem accordionItem title="生鲜">
      <AccordionContent>
        <List>
          <ListItem title="Item 1"></ListItem>
          <ListItem title="Item 2"></ListItem>
          <ListItem title="Item 3"></ListItem>
          <ListItem title="Item 4"></ListItem>
        </List>
      </AccordionContent>
    </ListItem>
    <ListItem accordionItem title="汽车摩托">
      <AccordionContent>
        <List>
          <ListItem title="Item 1"></ListItem>
          <ListItem title="Item 2"></ListItem>
          <ListItem title="Item 3"></ListItem>
          <ListItem title="Item 4"></ListItem>
        </List>
      </AccordionContent>
    </ListItem>
    <ListItem accordionItem title="医药">
      <AccordionContent>
        <List>
          <ListItem title="Item 1"></ListItem>
          <ListItem title="Item 2"></ListItem>
          <ListItem title="Item 3"></ListItem>
          <ListItem title="Item 4"></ListItem>
        </List>
      </AccordionContent>
    </ListItem>
  </List>

  </Page>
);
