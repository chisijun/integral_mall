import React from "react";
import {
  Page,
  List,
  Block,
  ListItem,
  Label,
  Input,
  BlockFooter,
  Subnavbar,
  Segmented,
  Button,
  Tabs,
  Tab,
  Row,
  Col,
  Navbar
} from "framework7-react";

export default class extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      username: "",
      password: ""
    };
  }

  render() {
    return (
      <Page loginScreen>
        {/* <LoginScreenTitle>Framework7</LoginScreenTitle> */}
        <Navbar title="用户登陆" backLink="Back">
          <Subnavbar>
            <Segmented raised>
              <Button tabLink="#username" tabLinkActive>
                账号密码登陆
              </Button>
              <Button tabLink="#code">短信验证码登陆</Button>
            </Segmented>
          </Subnavbar>
        </Navbar>
        <Tabs>
          <Tab id="username" tabActive className="page-content">
            <Block>
              <List form>
                <ListItem>
                  <Label>用户名</Label>
                  <Input
                    type="text"
                    placeholder="请输入你的用户名"
                    onInput={e => {
                      this.setState({ username: e.target.value });
                    }}
                  />
                </ListItem>
                <ListItem>
                  <Label>密码</Label>
                  <Input
                    type="password"
                    placeholder="请输入你的密码"
                    onInput={e => {
                      this.setState({ password: e.target.value });
                    }}
                  />
                </ListItem>
              </List>
              <List>
                <Button color="red" onClick={this.signIn.bind(this)}>
                  登陆
                </Button>
                <Button color="red" onClick={this.signIn.bind(this)}>
                  一键登陆
                </Button>
                <BlockFooter>
                  <Row>
                    <Col>
                      <Button color="green">忘记密码</Button>
                    </Col>
                    <Col>
                      <Button color="green">手机快速注册</Button>
                    </Col>
                  </Row>
                </BlockFooter>
              </List>
            </Block>
          </Tab>
          <Tab id="code" className="page-content">
            <Block>
              <List form>
                <ListItem>
                  <Label>手机号码</Label>
                  <Input
                    type="number"
                    placeholder="请输入手机号码"
                    onInput={e => {
                      this.setState({ username: e.target.value });
                    }}
                  />
                </ListItem>
                <ListItem>
                  <Row>
                    <Label>验证码</Label>
                    <Col>
                      <Input
                        type="number"
                        placeholder="收到的验证码"
                        onInput={e => {
                          this.setState({ password: e.target.value });
                        }}
                      />
                    </Col>

                    <Col>
                      <Button color="green">获取验证码</Button>
                    </Col>
                  </Row>
                </ListItem>
              </List>
              <List>
                <Button color="red" onClick={this.signIn.bind(this)}>
                  登陆
                </Button>

                <Button color="red" onClick={this.signIn.bind(this)}>
                  一键登陆
                </Button>
                <BlockFooter>
                  <Row>
                    <Col>
                      <Button color="green">忘记密码</Button>
                    </Col>
                    <Col>
                      <Button color="green">手机快速注册</Button>
                    </Col>
                  </Row>
                </BlockFooter>
              </List>
            </Block>
          </Tab>
        </Tabs>
      </Page>
    );
  }
  signIn() {
    const self = this;
    const app = self.$f7;
    const router = self.$f7router;
    app.dialog.alert(
      `用户名: ${self.state.username}<br>密码: ${self.state.password}`,
      () => {
        router.back();
      }
    );
  }
}
