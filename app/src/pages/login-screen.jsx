import React from "react";
import {
  Navbar,
  Page,
  LoginScreen,
  List,
  ListItem,
  Block,
  Button,
  LoginScreenTitle,
  BlockFooter,
  ListButton
} from "framework7-react";

export default class extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      loginScreenOpened: false,
      username: "",
      password: ""
    };
  }
  render() {
    return (
      <Page>
        <Navbar title="应用分享" backLink="Back" />
        <Block>
          <p>
            分享须知<br />
            1.<br />
            2.<br />
            3.
          </p>
        </Block>

        <List>
          <ListItem link="/login-screen-page/" title="请先登陆" />
        </List>

        <Block>
          <Button raised big fill loginScreenOpen=".demo-login-screen">
            As iOS
          </Button>
        </Block>

        <Block>
          <Button
            raised
            big
            fill
            onClick={() => {
              this.setState({ loginScreenOpened: true });
            }}
          >
            As Android{" "}
          </Button>
        </Block>

        <LoginScreen
          className="demo-login-screen"
          opened={this.state.loginScreenOpened}
          onLoginScreenClosed={() => {
            this.setState({ loginScreenOpened: false });
          }}
        >
          <Page loginScreen>
            <LoginScreenTitle>App</LoginScreenTitle>
            <List form>
              <ListItem>
                {/* <Label>Username</Label>
                <Input type="text" placeholder="Your username" onInput={(e) => {
                  this.setState({ username: e.target.value});
                }}></Input>
              </ListItem>
              <ListItem>
                <Label>Password</Label>
                <Input type="password" placeholder="Your password" onInput={(e) => {
                  this.setState({ password: e.target.value});
                }}></Input> */}
              </ListItem>
            </List>
            <List>
              <ListButton onClick={this.signIn.bind(this)}>Sign In</ListButton>
              <BlockFooter>
                Some text about login information.<br />Lorem ipsum dolor sit
                amet, consectetur adipiscing elit.
              </BlockFooter>
            </List>
          </Page>
        </LoginScreen>
      </Page>
    );
  }
  signIn() {
    const self = this;
    const app = self.$f7;

    app.dialog.alert(
      `感谢分享 ${self.state.username}<br>祝您愉快 ${self.state.password}`,
      () => {
        app.loginScreen.close();
      }
    );
  }
}
