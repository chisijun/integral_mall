import React from "react";
import nav1 from "../img/nav1.png";
import nav2 from "../img/nav2.png";
import nav3 from "../img/nav3.png";
import sb1 from "../img/sb1.png";
import sb2 from "../img/sb2.png";
import sb3 from "../img/sb3.png";
import {
  Icon,
  BlockTitle,
  Subnavbar,
  Segmented,
  Tabs,
  Tab,
  NavRight,
  Page,
  Navbar,
  Block,
  Button,
  Card,
  CardHeader,
  CardContent,
  CardFooter,
  Link
} from "framework7-react";

export default () => (
  <Page pageContent={false}>
    <Navbar color="orange"title="优惠卷">
      <Subnavbar color="orange">
        <Segmented raised>
          <Button tabLink="#product_1" tabLinkActive>
            未使用
          </Button>
          <Button tabLink="#product_2">已使用</Button>
        </Segmented>
      </Subnavbar>
      <NavRight>
        <Link popupClose>关闭</Link>
      </NavRight>
    </Navbar>
    <Tabs>
      <Tab id="product_1" tabActive className="page-content">
        <Block>
          <Icon f7="优惠卷" size="30px" color="green" />
          <BlockTitle>500M流量包</BlockTitle>
          <Card className="demo-facebook-card">
            <CardHeader className="no-border">
              <div className="demo-facebook-avatar">
                <img
                  alt=""
                  src={nav3}
                  width="34"
                  height="34"
                />
              </div>
              <div className="demo-facebook-name">John Doe</div>
              <div className="demo-facebook-date">
                到期时间：{new Date().toLocaleDateString()}
              </div>
            </CardHeader>
            <CardContent>
              <p>What a nice photo i took yesterday!</p>
              <img
                alt=""
                src={sb3}
                width="100%"
              />
              <p className="likes">Likes: 112 Comments: 43</p>
            </CardContent>
            <CardFooter className="no-border">
              <Link>点赞</Link>
              <Link>评论</Link>
              <Link>分享</Link>
            </CardFooter>
          </Card>
          <Card className="demo-facebook-card">
            <CardHeader className="no-border">
              <div className="demo-facebook-avatar">
                <img
                  alt=""
                  src={nav3}
                  width="34"
                  height="34"
                />
              </div>
              <div className="demo-facebook-name">John Doe</div>
              <div className="demo-facebook-date">
                到期时间：{new Date().toLocaleDateString()}
              </div>
            </CardHeader>
            <CardContent>
              <p>What a nice photo i took yesterday!</p>
              <img
                alt=""
                src={sb3}
                width="100%"
              />
              <p className="likes">Likes: 112 Comments: 43</p>
            </CardContent>
            <CardFooter className="no-border">
              <Link>点赞</Link>
              <Link>评论</Link>
              <Link>分享</Link>
            </CardFooter>
          </Card>
          <Card className="demo-facebook-card">
                  <CardHeader className="no-border">
                    <div className="demo-facebook-avatar">
                      <img
                        alt=""
                        src={nav3}
                        width="34"
                        height="34"
                      />
                    </div>
                    <div className="demo-facebook-name">John Doe</div>
                    <div className="demo-facebook-date">Monday at 3:47 PM</div>
                  </CardHeader>
                  <CardContent padding={true}>
                  <p>What a nice photo i took yesterday!</p>
                    <img
                      alt=""
                      src={sb2}
                      width="100%"
                    />
                    <p className="likes">Likes: 112 Comments: 43</p>
                  </CardContent>
                  <CardFooter className="no-border">
                    <Link>Like</Link>
                    <Link>Comment</Link>
                    <Link>Share</Link>
                  </CardFooter>
                </Card>
                <Card className="demo-facebook-card">
                  <CardHeader className="no-border">
                    <div className="demo-facebook-avatar">
                      <img
                        alt=""
                        src={nav3}
                        width="34"
                        height="34"
                      />
                    </div>
                    <div className="demo-facebook-name">John Doe</div>
                    <div className="demo-facebook-date">Monday at 3:47 PM</div>
                  </CardHeader>
                  <CardContent padding={true}>
                  <p>What a nice photo i took yesterday!</p>
                    <img
                      alt=""
                      src={sb2}
                      width="100%"
                    />
                    <p className="likes">Likes: 112 Comments: 43</p>
                  </CardContent>
                  <CardFooter className="no-border">
                    <Link>Like</Link>
                    <Link>Comment</Link>
                    <Link>Share</Link>
                  </CardFooter>
                </Card>
        </Block>
      </Tab>

      <Tab id="product_2" className="page-content">
        <Block>
          <Icon f7="优惠卷" size="30px" color="gray" />
          <Icon icon="icon-home"></Icon>
          <BlockTitle>出行补偿卷</BlockTitle>
          <Card className="demo-facebook-card">
            <CardHeader className="no-border">
              <div className="demo-facebook-avatar">
                <img
                  alt=""
                  src={nav3}
                  width="34"
                  height="34"
                />
              </div>
              <div className="demo-facebook-name">John Doe</div>
              <div className="demo-facebook-date">Monday at 3:47 PM</div>
            </CardHeader>
            <CardContent padding={true}>
            <p>What a nice photo i took yesterday!</p>
              <img
                alt=""
                src={sb1}
                width="100%"
              />
              <p className="likes">Likes: 112 Comments: 43</p>
            </CardContent>
            <CardFooter className="no-border">
              <Link>点赞</Link>
              <Link>评论</Link>
              <Link>分享</Link>
            </CardFooter>
          </Card>
          <Card className="demo-facebook-card">
            <CardHeader className="no-border">
              <div className="demo-facebook-avatar">
                <img
                  alt=""
                  src={nav3}
                  width="34"
                  height="34"
                />
              </div>
              <div className="demo-facebook-name">John Doe</div>
              <div className="demo-facebook-date">Monday at 2:15 PM</div>
            </CardHeader>
            <CardContent>
              <p>What a nice photo i took yesterday!</p>
              <img
                alt=""
                src={sb2}
                width="100%"
              />
              <p className="likes">Likes: 112 Comments: 43</p>
            </CardContent>
            <CardFooter className="no-border">
              <Link>点赞</Link>
              <Link>评论</Link>
              <Link>分享</Link>
            </CardFooter>
          </Card>
          <Card className="demo-facebook-card">
                  <CardHeader className="no-border">
                    <div className="demo-facebook-avatar">
                      <img
                        alt=""
                        src={nav3}
                        width="34"
                        height="34"
                      />
                    </div>
                    <div className="demo-facebook-name">John Doe</div>
                    <div className="demo-facebook-date">Monday at 3:47 PM</div>
                  </CardHeader>
                  <CardContent padding={true}>
                  <p>What a nice photo i took yesterday!</p>
                    <img
                      alt=""
                      src={sb2}
                      width="100%"
                    />
                    <p className="likes">Likes: 112 Comments: 43</p>
                  </CardContent>
                  <CardFooter className="no-border">
                    <Link>Like</Link>
                    <Link>Comment</Link>
                    <Link>Share</Link>
                  </CardFooter>
                </Card>
                <Card className="demo-facebook-card">
                  <CardHeader className="no-border">
                    <div className="demo-facebook-avatar">
                      <img
                        alt=""
                        src={nav3}
                        width="34"
                        height="34"
                      />
                    </div>
                    <div className="demo-facebook-name">John Doe</div>
                    <div className="demo-facebook-date">Monday at 3:47 PM</div>
                  </CardHeader>
                  <CardContent padding={true}>
                  <p>What a nice photo i took yesterday!</p>
                    <img
                      alt=""
                      src={sb2}
                      width="100%"
                    />
                    <p className="likes">Likes: 112 Comments: 43</p>
                  </CardContent>
                  <CardFooter className="no-border">
                    <Link>Like</Link>
                    <Link>Comment</Link>
                    <Link>Share</Link>
                  </CardFooter>
                </Card>
        </Block>
      </Tab>
    </Tabs>
  </Page>
);
