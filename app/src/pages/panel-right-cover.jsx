import React from "react";
import nv_fy from "../img/nv-fy.jpg";
import seckill_1 from "../img/seckill_1.jpg";
import {
  List,
  Row,
  Col,
  Link,
  Block,
  ListItem,
  Page,
  Icon,
  Checkbox,
  Navbar
} from "framework7-react";

export default () => (
  <Page>
    <Navbar  title="购物车" />
    <Block>
      <Row>
        {/* <Col>
          <Button>全选</Button>
        </Col> */}
        <Col>
          <Checkbox name="checkbox-1" />
        </Col>

        <Col>
          <Icon textColor="red" f7="money_yen_fill"></Icon>&nbsp;&nbsp;
          <Link color="red">0.00</Link>
        </Col>
        <Col>
          <Link color="orange">结算</Link>
        </Col>
      </Row>
    </Block>

    <List mediaList>
      <ListItem
        checkbox
        defaultChecked
        name="demo-media-checkbox"
        title="Facebook"
        after="￥999"
        subtitle="New messages from John Doe"
        text="Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla sagittis tellus ut turpis condimentum, ut dignissim lacus tincidunt. Cras dolor metus, ultrices condimentum sodales sit amet, pharetra sodales eros. Phasellus vel felis tellus. Mauris rutrum ligula nec dapibus feugiat. In vel dui laoreet, commodo augue id, pulvinar lacus."
      >
        <img
          slot="media"
          alt=""
          src={nv_fy}
          width="80"
        />
      </ListItem>

      <ListItem
        checkbox
        name="demo-media-checkbox"
        title="John Doe (via Twitter)"
        after="17:11"
        subtitle="John Doe (@_johndoe) mentioned you on Twitter!"
        text="Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla sagittis tellus ut turpis condimentum, ut dignissim lacus tincidunt. Cras dolor metus, ultrices condimentum sodales sit amet, pharetra sodales eros. Phasellus vel felis tellus. Mauris rutrum ligula nec dapibus feugiat. In vel dui laoreet, commodo augue id, pulvinar lacus."
      >
        <img
          slot="media"
          alt=""
          src={seckill_1}
          width="80"
        />
      </ListItem>
      <ListItem
        checkbox
        name="demo-media-checkbox"
        title="Facebook"
        after="16:48"
        subtitle="New messages from John Doe"
        text="Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla sagittis tellus ut turpis condimentum, ut dignissim lacus tincidunt. Cras dolor metus, ultrices condimentum sodales sit amet, pharetra sodales eros. Phasellus vel felis tellus. Mauris rutrum ligula nec dapibus feugiat. In vel dui laoreet, commodo augue id, pulvinar lacus."
      >
        <img
          slot="media"
          alt=""
          src={nv_fy}
          width="80"
        />
      </ListItem>
      <ListItem
        checkbox
        name="demo-media-checkbox"
        title="John Doe (via Twitter)"
        after="15:32"
        subtitle="John Doe (@_johndoe) mentioned you on Twitter!"
        text="Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla sagittis tellus ut turpis condimentum, ut dignissim lacus tincidunt. Cras dolor metus, ultrices condimentum sodales sit amet, pharetra sodales eros. Phasellus vel felis tellus. Mauris rutrum ligula nec dapibus feugiat. In vel dui laoreet, commodo augue id, pulvinar lacus."
      >
        <img
          slot="media"
          alt=""
          src={seckill_1}
          width="80"
        />
      </ListItem>
      <ListItem
        checkbox
        name="demo-media-checkbox"
        title="John Doe (via Twitter)"
        after="15:32"
        subtitle="John Doe (@_johndoe) mentioned you on Twitter!"
        text="Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla sagittis tellus ut turpis condimentum, ut dignissim lacus tincidunt. Cras dolor metus, ultrices condimentum sodales sit amet, pharetra sodales eros. Phasellus vel felis tellus. Mauris rutrum ligula nec dapibus feugiat. In vel dui laoreet, commodo augue id, pulvinar lacus."
      >
        <img
          slot="media"
          alt=""
          src={seckill_1}
          width="80"
        />
      </ListItem>
      <ListItem
        checkbox
        name="demo-media-checkbox"
        title="John Doe (via Twitter)"
        after="15:32"
        subtitle="John Doe (@_johndoe) mentioned you on Twitter!"
        text="Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla sagittis tellus ut turpis condimentum, ut dignissim lacus tincidunt. Cras dolor metus, ultrices condimentum sodales sit amet, pharetra sodales eros. Phasellus vel felis tellus. Mauris rutrum ligula nec dapibus feugiat. In vel dui laoreet, commodo augue id, pulvinar lacus."
      >
        <img
          slot="media"
          alt=""
          src={nv_fy}
          width="80"
        />
      </ListItem>
      <ListItem
        checkbox
        name="demo-media-checkbox"
        title="John Doe (via Twitter)"
        after="15:32"
        subtitle="John Doe (@_johndoe) mentioned you on Twitter!"
        text="Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla sagittis tellus ut turpis condimentum, ut dignissim lacus tincidunt. Cras dolor metus, ultrices condimentum sodales sit amet, pharetra sodales eros. Phasellus vel felis tellus. Mauris rutrum ligula nec dapibus feugiat. In vel dui laoreet, commodo augue id, pulvinar lacus."
      >
        <img
          slot="media"
          alt=""
          src="http://img.mp.sohu.com/upload/20170623/63cff86ee06241c8adf49b19620a2d6e_th.png"
          width="80"
        />
      </ListItem>
    </List>
  </Page>
);
