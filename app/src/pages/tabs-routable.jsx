import React from "react";
import nav1 from "../img/nav1.png";
import nav2 from "../img/nav2.png";
import nav3 from "../img/nav3.png";
import sb1 from "../img/sb1.png";
import sb2 from "../img/sb2.png";
import sb3 from "../img/sb3.png";
import img1 from "../img/nav_1.png";
import img2 from "../img/nav_2.png";
import img3 from "../img/nav_3.png";
import {
  Navbar,
  Block,
  Row,
  Button,
  Col,
  Icon,
  BlockTitle,
  Card,
  CardHeader,
  CardContent,
  CardFooter,
  Page,
  Tabs,
  Tab,
  Link,
  Toolbar
} from "framework7-react";
export default () => (
  <Page pageContent={false} colorTheme="orange">
    <Navbar title="推荐有奖" backLink="Back" />
    <Toolbar tabbar>
      <Link tabLink href="./" routeTabId="student">
        推荐学生
      </Link>
      <Link tabLink href="tab2/" routeTabId="teacher">
        推荐老师
      </Link>
      <Link tabLink href="tab3/" routeTabId="company">
        推荐企业
      </Link>
    </Toolbar>
    <Tabs routable>
      <Tab className="page-content" id="student">
        <BlockTitle />
        <Card className="demo-facebook-card">
          <CardHeader className="no-border">
            <div className="demo-facebook-avatar">
              <img alt="" src={nav1} width="34" height="34" />
            </div>
            <div className="demo-facebook-name">John Doe</div>
            <div className="demo-facebook-date">Monday at 3:47 PM</div>
          </CardHeader>
          <CardContent style={{ textAlign: "center" }}>
            <span>全国&gt;</span>
            <p>
              邀请好友，享首单8元立减<br />
              您得<strong>10元</strong>奖卷
            </p>
            <img alt="" src={sb1} width="100%" />
            <span>点击查看奖励规则&gt;</span>
            <p>马上邀请好友得奖励</p>
          </CardContent>
          <CardFooter style={{ margin: "0 5%" }} className="no-border">
            <Icon color="green" f7="chats_fill" />
            <Icon color="green" f7="persons" />
            <Icon color="blue" f7="data_fill" />
            <Icon color="orange" f7="data" />
          </CardFooter>
          <CardFooter className="no-border">
            <span>微信好友</span>
            <span>微信朋友圈</span>
            <span>二维码分享</span>
            <span>更多</span>
          </CardFooter>
          <CardFooter className="no-border">
            <span>已获得奖励：0元</span>
            <span>去看看&gt;</span>
          </CardFooter>
        </Card>
      </Tab>
      <Tab className="page-content" id="teacher">
        <BlockTitle />
        <Card className="demo-facebook-card">
          <CardHeader className="no-border">
            <div className="demo-facebook-avatar">
              <img alt="" src={nav1} width="34" height="34" />
            </div>
            <div className="demo-facebook-name">John Doe</div>
            <div className="demo-facebook-date">Monday at 3:47 PM</div>
          </CardHeader>
          <CardContent style={{ textAlign: "center" }}>
            <span>江门市&gt;</span>
            <p>
              邀请好友成为共享教师<br />
              完成3单各奖<strong>150元</strong>
            </p>
            <img alt="" src={sb2} width="100%" />
            <span>点击查看奖励规则&gt;</span>
            <CardFooter className="no-border">
              <Button fill color="gray">
                分享二维码
              </Button>
              <Button fill>马上邀请好友加入</Button>
            </CardFooter>
          </CardContent>
          <CardFooter className="no-border">
            <span>查看已获得奖励&gt;</span>
          </CardFooter>
        </Card>
      </Tab>
      <Tab className="page-content" id="company">
        <BlockTitle />
        <Card className="demo-facebook-card">
          <CardHeader className="no-border">
            <div className="demo-facebook-avatar">
              <img alt="" src={nav1} width="34" height="34" />
            </div>
            <div className="demo-facebook-name">John Doe</div>
            <div className="demo-facebook-date">Monday at 3:47 PM</div>
          </CardHeader>
          <CardContent style={{ textAlign: "center" }}>
            <p>
              邀请新企业充值1000元<br />
              您得<strong>100元</strong>出行卡
            </p>
            <span>最高得iPhoneX</span>
            <img alt="" src={sb1} width="100%" />
            <p>
              累计成功邀请10个以上企业充值激活<br />额外再得以下实物奖励
            </p>
          </CardContent>
          <Block>
            <Row noGap>
              <Col>
                <a>
                  <img slot="media" alt="" src={img1} width="100%" />
                  <strong>10个</strong>
                </a>
              </Col>
              <Col>
                <a>
                  <img slot="media" alt="" src={img2} width="100%" />
                  <strong>20个</strong>
                </a>
              </Col>
              <Col>
                <a>
                  <img slot="media" alt="" src={img3} width="100%" />
                  <strong>30个</strong>
                </a>
              </Col>
            </Row>
          </Block>
          <CardFooter className="no-border">
           
            <span>查看奖励&gt;</span>
          </CardFooter>
          <CardFooter className="no-border">
              <Button fill color="gray">
                已邀请注册企业
              </Button>
              <Button fill>已获得奖励</Button>
            </CardFooter>
          <CardFooter className="no-border">
           
            <span>查看奖励&gt;</span>
          </CardFooter>
          <CardContent style={{ textAlign: "center" }}>
          <Icon color="green" f7="persons" />
            <p><strong>- 什么是师霸企业版 -<br/></strong>实力助教成就优秀学生
            </p>
            <p><strong>- 简化流程，提升效率 -<br/></strong>
            统一开票/企业支付/后台管理
            </p>
            <img alt="" src={sb1} width="100%" />
          </CardContent>
          <CardContent style={{ textAlign: "center" }}>
          <p>&nbsp;&nbsp;以往用车报销消耗了企业大量时间，滴滴企业版自动化管理工具，帮我们解决企业和报销得所有问题，让企业更专注核心业务发展。
           </p>
            <strong>- 鲁迅 -<br/></strong>
          </CardContent>
          <CardContent style={{ textAlign: "center" }}>
          <Icon color="green" f7="persons" />
            <p><strong>- 合规透明，节省用车费用 -</strong><br/>
            比养车，租车更便宜/价格与滴滴出行一致/行程记录可查
            </p>
            <img alt="" src={sb1} width="100%" />
            <p>&nbsp;&nbsp;“起初我们也担心，员工用车由企业支付，会不会乱打车。没想到滴滴走在我们前面，员工在什么时间、地点用什么车，这些都可以直接设置自动化管理工具，帮我们解决企业和报销得所有问题，让企业更专注核心业务发展。
           </p><br/>
            <strong>- 鲁迅 -</strong>
          </CardContent>
        </Card>
      </Tab>
    </Tabs>
  </Page>
);
