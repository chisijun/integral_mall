import React from "react";
import img1 from "../img/nav_1.png";
import img2 from "../img/nav_2.png";
import img3 from "../img/nav_3.png";
import img4 from "../img/nav_4.png";
import img5 from "../img/nav_5.png";
import img6 from "../img/nav_6.png";
import img7 from "../img/nav_7.png";
import img8 from "../img/nav_8.png";
import l1 from "../img/l1.jpg";
import l2 from "../img/l2.jpg";
import l3 from "../img/l3.jpg";
import l4 from "../img/l4.jpg";
import l5 from "../img/l5.jpg";
import l6 from "../img/l6.jpg";
import l7 from "../img/l7.jpg";
import l8 from "../img/l8.jpg";
import nav1 from "../img/nav1.png";
import nav2 from "../img/nav2.png";
import nav3 from "../img/nav3.png";
import nav4 from "../img/nav4.png";
import nav5 from "../img/nav5.png";
import nav6 from "../img/nav6.png";
import nav7 from "../img/nav7.png";
import nav8 from "../img/nav8.png";
import sb1 from "../img/sb1.png";
import sb2 from "../img/sb2.png";
import sb3 from "../img/sb3.png";
// import swiper from "../swiper.min.js";

import {
  BlockTitle,
  ListItem,
  Block,
  Swiper,
  SwiperSlide,
  CardHeader,
  Page,
  Navbar,
  Icon,
  Row,
  Col,
  Button
} from "framework7-react";
export default () => (
  <Page colorTheme="gray">
    <Navbar title="积分商城" color="red" />
    <CardHeader className="no-border">
      <div className="demo-facebook-avatar">
        <i class="f7-icons">person</i>
      </div>
      <div className="demo-facebook-date">
        <Icon f7="money_yen_fill" />
      </div>
    </CardHeader>
    <Block>
      <Swiper
        autopaly={true}
        pagination
        navigation
        className="demo-swiper demo-swiper-coverflow "
        params={{
          effect: "coverflow",
          centeredSlides: true,
          loop: "true",
          slidesPerView: "auto"
        }}
      >
        <SwiperSlide>
          <img src={l1} alt="" width="100%" />
        </SwiperSlide>
        <SwiperSlide>
          <img src={l2} alt="" width="100%" />
        </SwiperSlide>
        <SwiperSlide>
          <img src={l3} alt="" width="100%" />
        </SwiperSlide>
        <SwiperSlide>
          <img src={l4} alt="" width="100%" />
        </SwiperSlide>
        <SwiperSlide>
          <img src={l4} alt="" width="100%" />
        </SwiperSlide>
      </Swiper>
    </Block>
    <Row>
      <Col>
        <Button panelOpen="left" color="orange">
          分类浏览
        </Button>
      </Col>
      <Col>
        <Button popupOpen="#popup-product" color="green">
          优惠卷
        </Button>
      </Col>
      <Col>
        <Button panelOpen="right" color="red">
          购物车
        </Button>
      </Col>
    </Row>
    <br />
    <Row>
      <Col>
        <a href="/login-screen-page/">
          <img slot="media" alt="" src={nav1} width="100%" />
          出行卷
        </a>
      </Col>
      <Col>
        <a href="/tabs-routable/">
          <img slot="media" alt="" src={nav2} width="100%" />
          本地生活
        </a>
      </Col>
      <Col>
        <a href="/">
          <img slot="media" alt="" src={nav3} width="100%" />
          流量包
        </a>
      </Col>
      <Col>
        <a href="/">
          <img slot="media" alt="" src={nav4} width="100%" />
          商旅特惠
        </a>
      </Col>
      <Col>
        <a href="/login-screen/">
          <img slot="media" alt="" src={nav5} width="100%" />
          其他商品
        </a>
      </Col>
    </Row>
    <BlockTitle textColor="orange">精品推荐</BlockTitle>
    <Block>
      <Swiper
        navigation
        params={{
          loop: "true",
          speed: 500,
          slidesPerView: 4,
          spaceBetween: 20,
          autopaly: "true"
        }}
      >
        <SwiperSlide>
          <ListItem>
            <img slot="media" alt="" src={l1} width="100%" />
          </ListItem>
        </SwiperSlide>
        <SwiperSlide>
          <ListItem>
            <img slot="media" alt="" src={l2} width="100%" />
          </ListItem>
        </SwiperSlide>
        <SwiperSlide>
          <ListItem>
            <img slot="media" alt="" src={l3} width="100%" />
          </ListItem>
        </SwiperSlide>
        <SwiperSlide>
          <ListItem>
            <img slot="media" alt="" src={l4} width="100%" />
          </ListItem>
        </SwiperSlide>
        <SwiperSlide>
          <ListItem>
            <img slot="media" alt="" src={l5} width="100%" />
          </ListItem>
        </SwiperSlide>
        <SwiperSlide>
          <ListItem>
            <img slot="media" alt="" src={l6} width="100%" />
          </ListItem>
        </SwiperSlide>
        <SwiperSlide>
          <ListItem>
            <img slot="media" alt="" src={l7} width="100%" />
          </ListItem>
        </SwiperSlide>
        <SwiperSlide>
          <ListItem>
            <img slot="media" alt="" src={l8} width="100%" />
          </ListItem>
        </SwiperSlide>
      </Swiper>
    </Block>
    <BlockTitle textColor="green">活动专区</BlockTitle>
    <Block>
      <Row noGap style={{ borderBottom: "1px solid #ccc" }}>
        <Col>
          <a>
            <img slot="media" alt="" src={sb1} width="100%" />
            <strong>教育精英</strong>
            <br />
            <span>
              20元师霸卷<br />
              <i style={{ color: "#ff9b25" }}>$99</i>明誉币
            </span>
          </a>
        </Col>
        <Col>
          <a>
            <img slot="media" alt="" src={sb2} width="100%" />
            <strong>教育精英</strong>
            <br />
            <span>
              20元师霸卷<br />
              <i style={{ color: "#ff9b25" }}>$99</i>明誉币
            </span>
          </a>
        </Col>
        <Col>
          <a>
            <img slot="media" alt="" src={sb3} width="100%" />
            <strong>教育精英</strong>
            <br />
            <span>
              20元师霸卷<br />
              <i style={{ color: "#ff9b25" }}>$99</i>明誉币
            </span>
          </a>
        </Col>
      </Row>
      <Row noGap>
        <Col>
          <a>
            <img slot="media" alt="" src={sb1} width="100" />
            <strong>教育精英</strong>
            <br />
            <span>
              20元师霸卷<br />
              <i style={{ color: "#ff9b25" }}>$99</i>明誉币
            </span>
          </a>
        </Col>
        <Col>
          <a>
            <img slot="media" alt="" src={sb2} width="100" />
            <strong>教育精英</strong>
            <br />
            <span>
              20元师霸卷<br />
              <i style={{ color: "#ff9b25" }}>$99</i>明誉币
            </span>
          </a>
        </Col>
        <Col>
          <a>
            <img slot="media" alt="" src={sb3} width="100%" />
            <strong>教育精英</strong>
            <br />
            <span>
              20元师霸卷<br />
              <i style={{ color: "#ff9b25" }}>$99</i>明誉币
            </span>
          </a>
        </Col>
      </Row>
    </Block>
    <BlockTitle textColor="green">为你推荐</BlockTitle>
    <Block>
      <Row noGap style={{ borderBottom: "1px solid #ccc" }}>
        <Col>
          <a>
            <img slot="media" alt="" src={sb1} width="100%" />
            <strong>教育精英</strong>
            <br />
            <span>
              20元师霸卷<br />
              <i style={{ color: "#ff9b25" }}>$99</i>明誉币
            </span>
          </a>
        </Col>
        <Col>
          <a>
            <img slot="media" alt="" src={sb2} width="100%" />
            <strong>教育精英</strong>
            <br />
            <span>
              20元师霸卷<br />
              <i style={{ color: "#ff9b25" }}>$99</i>明誉币
            </span>
          </a>
        </Col>
        <Col>
          <a>
            <img slot="media" alt="" src={sb3} width="100%" />
            <strong>教育精英</strong>
            <br />
            <span>
              20元师霸卷<br />
              <i style={{ color: "#ff9b25" }}>$99</i>明誉币
            </span>
          </a>
        </Col>
      </Row>
      <Row noGap>
        <Col>
          <a>
            <img slot="media" alt="" src={img1} width="100" />
            <strong>教育精英</strong>
            <br />
            <span>
              20元师霸卷<br />
              <i style={{ color: "#ff9b25" }}>$99</i>明誉币
            </span>
          </a>
        </Col>
        <Col>
          <a>
            <img slot="media" alt="" src={img2} width="100" />
            <strong>教育精英</strong>
            <br />
            <span>
              20元师霸卷<br />
              <i style={{ color: "#ff9b25" }}>$99</i>明誉币
            </span>
          </a>
        </Col>
        <Col>
          <a>
            <img slot="media" alt="" src={img3} width="100%" />
            <strong>教育精英</strong>
            <br />
            <span>
              20元师霸卷<br />
              <i style={{ color: "#ff9b25" }}>$99</i>明誉币
            </span>
          </a>
        </Col>
      </Row>
    </Block>
    <BlockTitle textColor="green">本地特惠</BlockTitle>
    <Block>
      <Row>
        <Col>
          <a>
            <img slot="media" alt="" src={sb2} width="100%" />
            <strong>教育精英</strong>
            <br />
            <span>
              20元师霸卷<br />
              <i style={{ color: "#ff9b25" }}>$99</i>明誉币
            </span>
          </a>
        </Col>
        <Col>
          <a>
            <img slot="media" alt="" src={sb3} width="100%" />
            <strong>教育精英</strong>
            <br />
            <span>
              20元师霸卷<br />
              <i style={{ color: "#ff9b25" }}>$99</i>明誉币
            </span>
          </a>
        </Col>
      </Row>
    </Block>
    <Block style={{ textAlign: "center" }}>
      <span>活动由师霸提供，与设备生产商Apple Inc.公司无关</span>
      <span>ShiBa商城法律声明</span>
      <br />
      <span>合作请联系：jifenshangcheng@shiba.com</span>
    </Block>
  </Page>
);
