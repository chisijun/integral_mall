// App.jsx
import React from "react";
import { App, Statusbar, Panel, View, Popup} from "framework7-react";
import routes from "./routes.js";
const f7Params = {
  // Array with app routes
  theme: "auto",
  // App Name
  name: "My App",
  // App id
  id: "com.myapp.test"
  // ...
};
export default () => (
  // Main Framework7 App component where we pass Framework7 params
  <App params={f7Params} routes={routes}>
    <Statusbar />
    <Panel left cover>
      <View url="/panel-left-cover/" />
    </Panel>
    <Popup id="popup-product" colorTheme="gray">
      <View url="/popup-product-detail/" />
    </Popup>
    <Panel right reveal>
      <View url="/panel-right-cover/" />
    </Panel>
    <View main url="/" />
  </App>
);
