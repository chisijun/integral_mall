import Home from "./pages/home.jsx";
import LoginScreen from "./pages/login-screen.jsx";
import LoginScreenPage from "./pages/login-screen-page.jsx";
import PanelLeftCover from "./pages/panel-left-cover.jsx";
import PanelRightCover from "./pages/panel-right-cover.jsx";
import PopupProductDetail from "./pages/popup-product-detail.jsx";
import TabsRoutable from "./pages/tabs-routable.jsx";
import NotFound from "./pages/404.jsx";
export default [
  // Index page
  {
    path: "/",
    component: Home
  },
  // About page
  {
    path: "/tabs-routable/",
    component: TabsRoutable,
    tabs: [
      {
        path: "/",
        id: "student",
        content: ``
      },
      {
        path: "/tab2/",
        id: "teacher",
        content: ``
      },
      {
        path: "/tab3/",
        id: "company",
        content: ``
      }
    ]
  },

  {
    path: "/login-screen/",
    component: LoginScreen
  },
  {
    path: "/login-screen-page/",
    component: LoginScreenPage
  },
  {
    path: "/panel-left-cover/",
    component: PanelLeftCover
  },
  {
    path: "/panel-right-cover/",
    component: PanelRightCover
  },
  {
    path: "/popup-product-detail/",
    component: PopupProductDetail
  },
  {
    path: "(.*)",
    component: NotFound
  }
];
