import React from "react";
import ReactDOM from "react-dom";
import "./App.css";
import "../node_modules/framework7/css/framework7.min.css";
import Framework7 from "framework7/framework7.esm.bundle.js";
import Framework7React from "framework7-react";
import App from "./App.jsx";
Framework7.use(Framework7React);
ReactDOM.render(<App />, document.getElementById("app"));

